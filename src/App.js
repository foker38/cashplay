import React, { Component } from "react";
import { Route, Link, withRouter, Switch } from "react-router-dom";
import { inject, observer } from "mobx-react";
import LazyRoute from "lazy-route";
// import DevTools from "mobx-react-devtools";

import TopBar from "components/TopBar";
import axiosConfig from 'config/axios';

@withRouter
@inject("userState")
@observer
export default class App extends Component {
    constructor(props) {
        super(props);
        this.userState = this.props.userState;
    }
    componentDidMount() {
        this.userState.checkSession();
    }
    render() {
        const {
            authenticated,
            authenticating,
            sessionData
        } = this.userState;

        console.log('rendering of app');

        return (
			<div className="wrapper">
				<TopBar />
				<Route
					exact
					path="/"
					render={props => (
						<LazyRoute {...props} component={import("./containers/Home/Home")} />
					)}
				/>
				<Route
					exact
					path="/profile"
					render={props => (
						<LazyRoute {...props} component={import("./containers/Profile/Profile")} />
                    )}
				/>
				<Route
					exact
					path="/login"
					render={props => (
						<LazyRoute {...props} component={import("./containers/Login/Login")} />
					)}
				/>
				<Route
					exact
					path="/posts"
					render={props => (
						<LazyRoute component={import("./containers/SubPage/SubPage")} />
					)}
				/>

				<Route
					exact
					path="/posts/:id"
					render={props => (
						<LazyRoute {...props} component={import("./containers/SubItem/SubItem")} />
					)}
				/>
				<Route
					exact
					path="/register"
					render={props => (
						<LazyRoute {...props} component={import("./containers/Register/Register")} />
					)}
				/>
			</div>
        );
    }
}
