import createBrowserHistory from 'history/createBrowserHistory';
import { syncHistoryWithStore } from 'mobx-react-router';
import * as kek from  'mobx-react-router';
import RouterStore from '../stores/RouterStore';


console.log(kek);

const browserHistory = createBrowserHistory();
const history = syncHistoryWithStore(browserHistory, new RouterStore());
export default history;