import("./styles/main.scss");
import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "mobx-react";
import { AppContainer } from "react-hot-loader";
import { rehydrate } from "rfx-core";


import graphqlClient from './utils/graphql';



import App from "./App";
import stores from "./stores/stores";

const store = rehydrate();

const renderApp = Component => {
    ReactDOM.render(
		<AppContainer>
			<Router>
				<ApolloProvider client={graphqlClient}>
					<Provider {...store}>
						<Component {...store} />
					</Provider>
				</ApolloProvider>
			</Router>
		</AppContainer>,
        document.getElementById("root")
    );
};

console.log(graphqlClient);
// graphqlClient.mutate({ mutation: { gql`{

// }`} });

// graphqlClient.query({ query: gql`{ hello }` }).then(console.log);



renderApp(App);

if (module.hot) {
    module.hot.accept(() => renderApp(App));
}
