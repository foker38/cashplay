import { observable, action } from "mobx";


import statApi from 'api/statApi';

export default class AppState {
  @observable statGames;
  @observable authenticated;
  @observable authenticating;
  @observable items;
  @observable item;


  constructor() {
    this.items = [];
    this.item = {};
    this.statGames = [];
  }

  async getStatGames() {
    let { data } = statApi.getStatGames();
    this.statGames = data;
  }

}
