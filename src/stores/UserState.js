import { observable, action } from "mobx";
import Cookies from 'js-cookie';
import history from '../utils/history';

import RouterStore from './RouterStore';

var routerStore = new RouterStore();


import userApi from 'api/userApi';


export default class AppState {
  @observable authenticated;
  @observable authenticating;
  @observable sessionData;


  constructor() {
    this.authenticated = false;
    this.authenticating = false;
    this.sessionData = undefined;
  }

  @action async checkSession(){
    const data = localStorage.getItem('token');
    if (data) {
      const { data } = await userApi.getMe();
      this.createSession(data);

    }
  }

  @action async authenticate({ email, password }) {
      const { data } = await userApi.authenticate({ email, password });

      localStorage.setItem('token', data.token);
      this.createSession(data.user);
  }

   @action async register({ lastName, firstName, login, password, phone, email }) {
    const data = await userApi.register({ lastName, firstName, login, password, phone, email });
    this.statGames = data;
  }


  @action createSession(sessionData) {
    console.log('creating session', sessionData)
    this.sessionData = sessionData;
    this.autheticated = true;
    setTimeout(()=> {
        console.log(routerStore);
        window.routerStore = routerStore;
        // routerStore.go('/');
    });
  }

  @action logout() {
    localStorage.removeItem('token');
    this.sessionData = undefined;
    this.authenticated = false;
  }



}
