import { store } from "rfx-core";
import { RouterStore } from 'mobx-react-router';


import AppState from "./AppState";
// import RouterStore from './RouterStore';
import UserState from "./UserState";

export default store.setup({
	appState: AppState,
	userState: UserState,
	routing: RouterStore
});