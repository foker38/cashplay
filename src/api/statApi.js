import axios from "axios";

export default {
    getStatGames() {
        // return await axios.get(
        //     '/stat/games'
        // );
        return {
            data: [
                {
                    id: 172387,
                    type: '1x1',
                    status: 'finished',
                    game: 'dota2',
                    time: new Date().getTime(),
                    bet: 10000
                },
                {
                    id: 28378927384,
                    type: '5x5',
                    status: 'finished',
                    game: 'dota2',
                    time: new Date().getTime(),
                    bet: 100000
                }
            ]
        };
    }
};

