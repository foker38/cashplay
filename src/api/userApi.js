import axios from 'axios';

export default {
    async register(form) {
        console.log(form);
        // graphql.mutation({ variables: form });
        // return await axios.post('/user', form);
    },

    async authenticate(form) {
        return await axios.post('/auth', form);
    },

    async getMe() {
        return await axios.get('/user/me');
    },

    async authSteam() {
        return await axios.get('/auth/steam')
    }
};

