import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { Match, Link } from "react-router-dom";


@observer
export default class SubPage extends Component {
	constructor(props) {
		super(props);
		this.store = this.props.store;

	}
	render() {
		const { items } = [{

		}];
		return (
			<div className="page posts">
				<h1>Posts</h1>
				<p className="subheader">
					Posts are fetched from jsonplaceholder.typicode.com
				</p>
				<hr />
				This is a page for registered users only
			</div>
		);
	}
}
