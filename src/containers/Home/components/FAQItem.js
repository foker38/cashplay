import React, { PureComponent } from "react";

export default class FAQitem extends PureComponent {

    render() {
        const { item } = this.props;

        return (
            <li className="faq-item">
                <h6 className="faq-item__title">{item.title}</h6>
                <span>{item.desc}</span>
            </li>
        );
    }
}
