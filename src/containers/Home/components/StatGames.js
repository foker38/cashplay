import React, { Component } from "react";


import('./StatGames.scss');

export default class StatGames extends Component {

    render() {
        const { games } = this.props;

        return (
            <table className="stat-games">
                <thead>
                    <tr className="stat-games__item">
                        <td>Тип</td>
                        <td>Ставка</td>
                        <td>Статус</td>
                        <td>Время</td>
                        <td>Игра</td>
                    </tr>
                </thead>
                <tbody>
                {games && games.map(game => (<tr className="stat-games__item" key={game.id}>
                    <td>{game.type}</td>
                    <td>{game.bet}</td>
                    <td>{game.status}</td>
                    <td>{game.time}</td>
                    <td>{game.game}</td>
                </tr>))}
                </tbody>
            </table>
        );
    }
}
