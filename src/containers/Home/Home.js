import React, { Component } from "react";
import { inject, observer } from "mobx-react";

import ActiveLink from "components/ui/ActiveLink";

import StatGames from "./components/StatGames";
import FAQ from "./components/FAQ";



@inject("appState")
@observer
export default class Home extends Component {
	constructor(props) {
		super(props);

        this.props.appState.getStatGames();
	}

	render() {
		return (
			<div className="page home">
				<header>
					<div className="hero-unit">
						<h1>Зарабатывай деньги своими матчами по Dota 2</h1>
					</div>
					<div className="hero-subunit">
						<h4>
							Делай ставки на свои матчи, играй и выигрывай реальные деньги
						</h4>
					</div>
					<div className="start-buttons">
						<ActiveLink activeOnlyWhenExact={true} to="/register">Начать играть</ActiveLink>
					</div>
				</header>
				<main>
					<div className="section-header">
						<h3>Последние сыгранные матчи</h3>
						<hr />
						<div className="w100p">
							<StatGames games={this.props.appState.statGames} />
						</div>
					</div>
					<div className="section-header">
						<h3>Последние сыгранные матчи</h3>
						<hr />
						<div className="w100p">
							<FAQ />
						</div>
					</div>
				</main>
			</div>
		);
	}
}
