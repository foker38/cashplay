import React, { Component } from "react";
import { inject, observer } from "mobx-react";


@inject("appState")
@observer
export default class Home extends Component {
    constructor(props) {
        super(props);

        this.props.appState.getStatGames();
    }

    render() {
        return (
            <div className="page home">
                <main>
                    здесь профиль пользователя
                </main>
            </div>
        );
    }
}
