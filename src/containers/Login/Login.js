import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { Form } from 'react-form'

import Input from 'components/form/Input';


@inject("userState")
@observer
export default class Login extends Component {

    authenticate = (values) => {
        this.props.userState.authenticate(values);
    };

    renderForm = ({submitForm}) => {
        return (
            <div className="page page_800">
                <form className="form" onSubmit={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    submitForm(e);
                }}>
                    <Input className="form__input" field="email" label="Эл. почта" />
                    <Input className="form__input" field="password" label="Пароль" type="password" />
                    <button className="form__submit">Войти</button>
                </form>
            </div>
        );
    };

    render(){
        console.log(this.props.history.push, 3434434);
        setTimeout(()=>{
            // this.props.history.push('/posts')
        });
        return (
            <Form
                onSubmit={this.authenticate}
                validate={({ password, email }) => {
                    return {
                        email: !email ? 'An email is required' : undefined,
                        password: !password ? 'A password is required' : undefined
                    }
                }}
            >
                {this.renderForm}
            </Form>
        );
    }
}