import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Form } from 'react-form';
import { Mutation } from "react-apollo";


// console.log(Mutation);

import Input from 'components/form/Input';

import gql from 'graphql-tag';

var t = gql`
             mutation createUser($user:CreateUser!) {
              createUser(user:$user) {
                id
              }
            }
        `;


@inject("userState")
@observer
export default class Register extends Component {

    // register = (values) => {
    //     console.log('success');
    //     this.props.userState.register(values);
    // };

    renderForm = ({ submitForm }) => {
        return (
            <div className="page page_800">
                <form className="form" onSubmit={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    submitForm(e);
                }}>
                    <Input className="form__input" field="firstName" label="Имя"/>
                    <Input className="form__input" field="lastName" label="Фамилия"/>
                    <Input className="form__input" field="login" label="Логин"/>
                    <Input className="form__input" field="email" label="эл. почта"/>
                    <Input className="form__input" field="password" label="Пароль" type="password"/>
                    {/*<Input className="form__input" field="phone" label="телефон"/>
                    <Input className="form__input" field="firstName" label="Имя"/>
                    <Input className="form__input" field="lastName" label="Фамилия"/>*/}
                    <button className="form__submit">Зарегистрироваться</button>
                </form>
            </div>
        );
    };

    render() {
        return (
            <Mutation mutation={t}>
                {(createUser, { data, loading, error }) => {
                    if (data) {
                        return (<div>`ну привет пидор ${data.id}`</div>);
                    } else return (
                        <div>

                            <Form
                                onSubmit={(values)=> {
                                    createUser({ variables: { user: values } })
                                }}
                                validate={({ password, email }) => {
                                    return {
                                        password: !password ? 'A password is required' : undefined,
                                        email: !email ? 'An email is required' : undefined,
                                    }
                                }}
                            >
                                {this.renderForm}
                            </Form>
                        </div>
                    )
                }}
            </Mutation>
        );
    }
}