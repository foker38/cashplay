import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router-dom";
import ActiveLink from "components/ui/ActiveLink";


import Button from "./ui/Button";

@withRouter
@inject("appState")
@observer
export default class TopBar extends Component {
	constructor(props) {
		super(props);
		this.store = this.props.appState;
	}

	authenticate(e) {
		if (e) e.preventDefault();
		console.log("CLICKED BUTTON");
	}

	render() {
		const { authenticated } = this.store;
		return (
			<div className="topbar">
				<nav>
					<ActiveLink activeOnlyWhenExact={true} to="/">Home</ActiveLink>
                    {authenticated && <ActiveLink to="/posts">Posts</ActiveLink>}
				</nav>
				{
					authenticated &&
					<Button
						onClick={this.authenticate.bind(this)}
						title={"Log out"}
					/>
				}
				{
					!authenticated && <ActiveLink className="button" to="/login" >Sign in</ActiveLink>
				}
                {
                    !authenticated && <ActiveLink className="button" to="/register" >Register</ActiveLink>
                }
			</div>
		);
	}
}
