import React, { Component } from 'react';

import { FormInput } from 'react-form';


export default class Input extends Component {

    render(){
        const { field, className, label, ...rest } = this.props;
        return (
            <FormInput className={className} field={field}>
                {({setValue, getValue, setTouched, getError, ...props}) => {
                    console.log(props);
                    return (
                        <div className="control control_text">
                            { label && <label htmlFor={field}>{label}</label> }
                            <input
                                id={field}
                                name={field}
                                {...rest} // Send the rest of your props to React-Select
                                value={getValue()} // Set the value to the forms value
                                onChange={({ target: { value }}) => setValue(value)} // On Change, update the form value
                                onBlur={() => setTouched()} // And the same goes for touched
                            />
                            {/*{getError() && <div className="control__error">{getError(field)}</div>}*/}
                        </div>
                    )
                }}
            </FormInput>

        )
    }
}