import React, { Component } from 'react';
import { FormInput } from 'react-form';


export default class FormControl extends Component {
    render() {
        const { children, field, ...rest } = this.props;
        var Children = children;
        return (
            <FormInput field={field}>
                {({setValue, getValue, setTouched, ...props}) => {console.log(props);return (
                    <Children
                        {...rest}
                        setValue={setValue}
                        getValue={getValue}
                        setTouched={setTouched}
                    />
                )}}
            </FormInput>
        )
    }
}